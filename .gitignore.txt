# IntelliJ folders #
.idea/ 
enunt.md 
target/ 
*.iml
# Class Files #
*.class
# Package Files #
*.jar
*.war
*.ear
*.txt
nbproject/
photos/

