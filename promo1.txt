
Să știți, iubiți credincioși, că atunci când Domnul Hristos a rânduit lucrul acesta, să iubim pe aproapele nostru, a avut în vedere 
pe orice om care ne vine în față, pe care într-un fel, Dumnezeu ni-l trimite in față, Dumnezeu ni-l face aproape de noi, Dumnezeu 
ni-l aduce lânga noi. 
<br />
Spunea cineva, și pe bună dreptate, că cel mai însemnat om din lumea aceasta este omul de lângă tine, de care atârnă mântuirea ta. 
În Pateric se spune: „de la aproapele este viața și moartea că, dacă ajutăm pe aproapele nostru, lui Dumnezeu îi slujim, dacă-l 
smintim pe aproapele nostru, lui Hristos greșim". <br />
